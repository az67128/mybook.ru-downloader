const getreaderArgs = bookUrl => {
  return fetch(`${bookUrl}${bookUrl.indexOf('reader/') < 0 ? 'reader/' : ''}`)
    .then(res => res.text())
    .then(text => {
      const regExp = /window\.readerArgs = \{.*\}/gi;
      return JSON.parse(regExp.exec(text)[0].replace('window.readerArgs = ', ''));
    });
};

const getContainerXML = prefix => {
  return fetch(`https://mybook.ru${prefix}META-INF/container.xml`)
    .then(res => res.text())
    .then(data => data);
};

const getContentOpfUrl = containerXML => {
  const parser = new DOMParser();
  const xmlDoc = parser.parseFromString(containerXML, 'text/xml');
  return xmlDoc.getElementsByTagName('rootfile')[0].getAttribute('full-path');
};

const getContentOpf = (prefix, contentOpfUrl) => {
  return fetch(`https://mybook.ru${prefix}${contentOpfUrl}`)
    .then(res => res.text())
    .then(data => data);
};

const getContent = (prefix, contentOpfUrl, filePath) => {
  let tries = 4;
  return new Promise((resolve, reject) => {
    function getFile() {
      fetch(`https://mybook.ru${prefix}${contentOpfUrl.replace('content.opf', '')}${filePath}`)
        .then(res => {
          if (res.status === 503) throw new Error();

          const fileType = filePath.slice(-3);
          return fileType === 'jpg' || fileType === 'png' || fileType === 'otf'
            ? res.blob()
            : res.text();
        })
        .then(content => {
          resolve({ filePath, content });
        })
        .catch(err => {
          tries -= 1;
          if (tries > 0) {
            console.log('retry of', filePath);
            setTimeout(getFile, 500);
          } else {
            reject("can't get content");
          }
        });
    }
    getFile();
  });
};

const getContentArray = (prefix, contentOpfUrl, contentOpf) => {
  const parser = new DOMParser();
  const xmlDoc = parser.parseFromString(contentOpf, 'text/xml');
  const items = xmlDoc.getElementsByTagName('item');
  const links = [...items].map(item => item.getAttribute('href'));

  return new Promise((resolve, reject) => {
    let i = 0;
    const content = [];
    const getNext = () => {
      getContent(prefix, contentOpfUrl, links[i])
        .then(result => {
          content.push(result);
          i += 1;
          if (links[i]) {
            setTimeout(getNext, Math.random() * 500);
          } else {
            resolve(content);
          }
        })
        .catch(err => reject(err));
    };
    getNext();
  });
};

const makeEpub = ({ contentOpfUrl, contentArray, containerXml, contentOpf, readerArgs }) => {
  return new Promise((resolve, reject) => {
    const zip = new JSZip();
    const contentPath = contentOpfUrl
      .split('/')
      .slice(0, -1)
      .join('/');
    contentArray.forEach(item => {
      zip.file(`${contentPath}/${item.filePath}`, item.content),
        { binary: typeof item.content !== 'string' };
    });
    zip.file('META-INF/container.xml', containerXml);
    zip.file(contentOpfUrl, contentOpf);
    zip.file('mimetype', 'application/epub+zip');
    zip
      .generateAsync({ type: 'blob' })
      .then(function(content) {
        saveAs(content, readerArgs.name + '.epub');
        resolve('done');
      })
      .catch(err => reject(err));
  });
};

function* getBook(bookUrl) {
  const readerArgs = yield getreaderArgs(bookUrl);
  const containerXml = yield getContainerXML(readerArgs.prefix);

  const contentOpfUrl = getContentOpfUrl(containerXml);
  const contentOpf = yield getContentOpf(readerArgs.prefix, contentOpfUrl);
  const contentArray = yield getContentArray(readerArgs.prefix, contentOpfUrl, contentOpf);
  const result = yield makeEpub({
    contentOpfUrl,
    contentArray,
    containerXml,
    contentOpf,
    readerArgs,
  });
}

function execute(generator, yieldValue) {
  let next = generator.next(yieldValue);
  if (!next.done) {
    next.value.then(result => execute(generator, result), err => generator.throw(err));
  } else {
    document.querySelector('.downloadInformer').style.display = 'none';
  }
}

const dowloadBook = bookUrl => {
  document.querySelector('.downloadInformer').style.display = 'block';
  execute(getBook(bookUrl));
};

const insertButton = (targetNode, needOffset = false) => {
  const downloadButton = document.createElement('div');
  downloadButton.classList.add('downloadButton');
  downloadButton.style.padding = '0.5rem';
  downloadButton.style.background = 'linear-gradient(to bottom, #f7bf68, #f39c13)';
  downloadButton.style.borderRadius = '1rem';
  downloadButton.style.textAlign = 'center';
  downloadButton.style.position = 'relative';
  downloadButton.style.textAlign = 'center';
  if (needOffset) downloadButton.style.top = '-40px';
  downloadButton.style.fontSize = '14px';
  downloadButton.style.margin = '8px';
  downloadButton.innerHTML = 'скачать';
  downloadButton.addEventListener('click', e => {
    const bookUrl = e.target.parentElement.querySelector('a').getAttribute('href');
    dowloadBook(bookUrl);
  });
  targetNode.appendChild(downloadButton);
};

const insertInformer = () => {
  const informer = document.createElementff('div');
  informer.classList.add('downloadInformer');
  informer.style.position = 'fixed';
  informer.style.bottom = '0px';
  informer.style.left = '0px';
  informer.style.padding = '0.5rem';
  informer.style.width = '100%';
  informer.style.display = 'none';
  informer.style.zIndex = '99';
  informer.innerHTML = 'Загружаем книгу';
  informer.style.background = 'linear-gradient(to bottom, #f7bf68, #f39c13)';
  informer.style.textAlign = 'center';
  document.body.appendChild(informer);
};

const addMarkup = () => {
  setTimeout(addMarkup, 2000);
  if (document.querySelectorAll('.downloadButton').length > 0) return;
  [...document.querySelectorAll('.BookCover__book')].forEach(book => insertButton(book, true));
  [...document.querySelectorAll('.BookPageHeaderContent__bookReadButtons')].forEach(book =>
    insertButton(book)
  );
  if (document.querySelectorAll('.downloadInformer').length === 0) insertInformer();
};

addMarkup();
