## Mybook.ru books downloader

Chrome extension that allows to download books from [https://mybook.ru](https://mybook.ru).

You need to have subscription to download books.

Download from [chrome store](https://chrome.google.com/webstore/detail/cngjcklphdhankjhieennmjpjcedhjpc)

![demo image](/img/demo.png)

### Build with

- Vanila js
- JSZip
- filesaver
